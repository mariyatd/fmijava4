package bg.sofia.uni.fmi.mjt.smartcity.hub;

import bg.sofia.uni.fmi.mjt.smartcity.device.SmartDevice;
import bg.sofia.uni.fmi.mjt.smartcity.enums.DeviceType;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class SmartCityHub {
    private LinkedHashMap<String, SmartDevice> smartDevices;
    private Map<DeviceType, Integer> smartDevicesByType;

    public SmartCityHub() {
        this.smartDevices = new LinkedHashMap<>();
        this.smartDevicesByType = new EnumMap<>(DeviceType.class);
        //TODO iterate maybe? talk to Gabi
        this.smartDevicesByType.put(DeviceType.CAMERA, 0);
        this.smartDevicesByType.put(DeviceType.LAMP, 0);
        this.smartDevicesByType.put(DeviceType.TRAFFIC_LIGHT, 0);
    }

    public void register(SmartDevice device) throws DeviceAlreadyRegisteredException {
        if (device == null) {
            throw new IllegalArgumentException("Device cannot be null");
        }

        if (smartDevices.get(device.getId()) != null) {
            throw new DeviceAlreadyRegisteredException("Device with id " + device.getId() + " already exists.");
        }

        this.smartDevices.put(device.getId(), device);
        int previousDeviceQuantity = this.smartDevicesByType.get(device.getType());
        this.smartDevicesByType.put(device.getType(), ++previousDeviceQuantity);
    }

    public void unregister(SmartDevice device) throws DeviceNotFoundException {
        if (device == null) {
            throw new IllegalAccessError("Device cannot be null");
        }

        if (smartDevices.get(device.getId()) == null) {
            throw new DeviceNotFoundException("Device with id " + device.getId() + " not found.");
        }

        this.smartDevices.remove(device.getId());
        int previousDeviceQuantity = this.smartDevicesByType.get(device.getType());
        this.smartDevicesByType.put(device.getType(), --previousDeviceQuantity);
    }

    public SmartDevice getDeviceById(String id) throws DeviceNotFoundException {
        if (id == null) {
            throw new IllegalArgumentException("Id cannot be null");
        }

        SmartDevice device = smartDevices.get(id);
        if (device == null) {
            throw new DeviceNotFoundException("Device with id " + id + " not found");
        }

        return device;
    }

    public int getDeviceQuantityPerType(DeviceType type) {
        if (type == null) {
            throw new IllegalArgumentException("Type cannot be null");
        }
        int quantity = smartDevicesByType.get(type).intValue();
        return quantity;
    }

    public Collection<String> getTopNDevicesByPowerConsumption(int n) {
        if (n < 0) {
            throw new IllegalArgumentException("N shouldn't be negative");
        }

        List<SmartDevice> devices = smartDevices.values().stream().collect(Collectors.toList());
        LocalDateTime now = LocalDateTime.now();
        Collections.sort(devices, new Comparator<SmartDevice>() {
            @Override
            public int compare(SmartDevice o1, SmartDevice o2) {
                long o1Duration = Duration.between(o1.getInstallationDateTime(), now).toHours();
                long o2Duration = Duration.between(o2.getInstallationDateTime(), now).toHours();

                return (int) (o2Duration * o2.getPowerConsumption()) - (int) (o1Duration * o1.getPowerConsumption());
            }
        });

        Collection<String> result = new ArrayList<>();
        //TODO Sort ids not values by same compare, discuss with Gabi tomorrow
        n = n >= devices.size() ? devices.size() : n;

        for (int k = 0; k < n; k++) {
            result.add(devices.get(k).getId());
        }

        return result;
    }

    public Collection<SmartDevice> getFirstNDevicesByRegistration(int n) {
        if (n < 0) {
            throw new IllegalArgumentException("N should be positive");
        }

        n = n>= smartDevices.size() ? smartDevices.size() : n;

        //TODO talk to Gabi
        List<SmartDevice> firstRegistered = smartDevices.values().stream().limit(n).collect(Collectors.toList());

        return firstRegistered;
    }
}
