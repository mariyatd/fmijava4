package bg.sofia.uni.fmi.mjt.smartcity.device;

import bg.sofia.uni.fmi.mjt.smartcity.enums.DeviceType;

import java.time.LocalDateTime;

public class SmartLamp extends AbstractSmartDevice {

    private volatile static int idCounter = 0;

    public SmartLamp(String name, double powerConsumption, LocalDateTime installationDateTime) {
        super(name, powerConsumption, installationDateTime);
        this.type = DeviceType.LAMP;
        this.id = this.type.getShortName() + "-" + name + "-" + idCounter++;
    }
}
