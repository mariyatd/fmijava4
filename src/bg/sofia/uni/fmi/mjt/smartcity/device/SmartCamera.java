package bg.sofia.uni.fmi.mjt.smartcity.device;

import bg.sofia.uni.fmi.mjt.smartcity.enums.DeviceType;

import java.time.LocalDateTime;

public class SmartCamera extends AbstractSmartDevice {

    private static int idCounter = 0;

    public SmartCamera(String name, double powerConsumption, LocalDateTime installationDateTime) {
        super(name, powerConsumption, installationDateTime);
        this.type = DeviceType.CAMERA;
        this.id = this.type.getShortName() + "-" + this.getName() + "-" + idCounter++;
    }
}
